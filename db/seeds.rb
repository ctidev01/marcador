20.times do |i|
  User.create({email: "u#{i}@prueba.pp", password: "123123123", password_confirmation: "123123123"})
end

200.times do |i|
  p1_limit = (User.count/2)-1
  p2_limit = (User.count/2)
  p1 = User.offset(rand(0..p1_limit)).first
  p2 = User.offset(rand(p2_limit..User.count)).first
  if p1 != p2
    @match = Match.new
    @match.played_at = Time.zone.today - (rand(1..10)).days
    @match.scores.build(user: p1, points: rand(21..30))
    @match.scores.build(user: p2, points: rand(21..30))
    @match.save
  end
end
