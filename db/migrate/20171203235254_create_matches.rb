class CreateMatches < ActiveRecord::Migration[5.1]
  def change
    create_table :matches do |t|
      t.datetime :played_at

      t.timestamps null: false
    end
  end
end
