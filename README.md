# README

Se requiere hacer poner en producción una aplicación web construida con Ruby que usa Postgresql como base de datos.

Para ello se tiene un servidor Ubuntu 16.04 en el cual es necesario que en el server sea instalado versión 2.5.1 o superior de Ruby, instalar la gem para poder ejecutar Ruby on Rails 5.2.1.

Se requiere instalar un servidor web ya sea Apache o Nginx que combine con un servidor de aplicaciones ya sea Phusion Passenger o Capistrano

La prueba es poner en marcha en un server para que pueda funcionar la aplicación web.

El código de la aplicación se encuentra en el siguiente repositorio
